import { LocalStorage, Notify } from 'quasar'
import { route } from 'quasar/wrappers'
import { createRouter, createMemoryHistory, createWebHistory, createWebHashHistory } from 'vue-router'
import routes from './routes'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE)
  })

  // const isAuthenticated = false

  // Router.beforeEach((to, from, next) => {
  //   const isAuthenticated = JSON.parse(localStorage.getItem('authenticated'))
  //   if (to.name !== 'login' && !isAuthenticated) next({ name: 'login' })
  //   if (to.name === 'login' && isAuthenticated) next({ name: 'about' })

  //   else next()
  // })

  Router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.autentifikasi)) {
      // console.log(LocalStorage)
      // console.log(LocalStorage.getItem('datauser'))
      if (LocalStorage.getItem('datauser') === null || LocalStorage.getItem('datauser') === undefined) {
        next({
          path: '/auth'
        })
        Notify.create({
          icon: 'ion-close',
          color: 'negative',
          message: 'Anda belum login',
          actions: [{ icon: 'cloe', color: 'white' }]
        })
      } else {
        next()
      }
    } else {
      next()
    }
  })

  return Router
})
