
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    meta: {
      autentifikasi: true
    },
    children: [
      { path: '', name: 'index', component: () => import('pages/Index.vue') },
      { path: 'about', name: 'about', component: () => import('pages/About.vue') },
      { path: 'Contact', name: 'contact', component: () => import('pages/Contact.vue') },
      { path: 'home', name: 'home', component: () => import('pages/Home.vue'), beforeEnter: (to, from) => console.log('ini mo ke home') },
      // { path: 'roles', name: 'roles', component: () => import('pages/Roles.vue') },
      // { path: 'roles/new', name: 'roles-new', component: () => import('pages/RoleNew.vue') },
      { path: 'vfor', name: 'vfor', component: () => import('pages/Vfor.vue') },
      { path: 'tables', name: 'vfor', component: () => import('pages/Tables.vue') },
      { path: 'method', name: 'method', component: () => import('pages/Methode.vue') },
      { path: 'method2', name: 'method2', component: () => import('pages/Methode2.vue') },
      { path: 'computed', name: 'computed', component: () => import('pages/Computed.vue') },
      { path: 'computed-get', name: 'computedget', component: () => import('pages/ComputedGetSet.vue') },
      { path: 'komponen', name: 'komponen', component: () => import('pages/Komponen.vue') },
      { path: 'komponen-context', name: 'komponencontext', component: () => import('pages/KomponenContext.vue') },
      { path: 'roles/new2', name: 'komponencontext', component: () => import('pages/RoleNew2.vue') },
      { path: 'placeholder', name: 'placeholder', component: () => import('pages/PlaceHolder.vue') },
      { path: 'subhalaman', name: 'subhalaman', component: () => import('pages/HalamanSetup.vue') },
      { path: 'roles', name: 'roles', component: () => import('pages/module/Roles/Roles.vue') },
      { path: 'roles/new', name: 'roles-new', component: () => import('pages/module/Roles/RoleNew.vue') },
      { path: 'roles/edit/:id', name: 'roles-edit', component: () => import('pages/module/Roles/RoleEdit.vue') },
      { path: 'roles/delete/:id', name: 'roles-delete', component: () => import('pages/module/Roles/RoleDelete.vue') }
    ]
  },
  {
    path: '/auth',
    component: () => import('layouts/auth/Auth.vue'),
    children: [
      { path: '', name: 'login', component: () => import('pages/auth/Login.vue') },
      { path: 'login2', name: 'login2', component: () => import('pages/auth/Login2.vue') },
      { path: 'register', name: 'register', component: () => import('pages/auth/Register.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
